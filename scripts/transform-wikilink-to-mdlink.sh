#!/bin/bash
find "${1}" -regextype posix-extended -regex ".*[0-9]{14}.*\.md" -exec sed -i -E "s/\[\[([0-9]{14})\]\]/[\1](\1)/g" {} \;
