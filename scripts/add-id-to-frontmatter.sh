#!/bin/bash
find "${1}" -regextype posix-extended -regex ".*[0-9]{14}.*\.md" -exec sh -c '
    file="$1"
    echo $file
    yq -i --front-matter=process ".id=$(basename $file .md)| . |= pick( ([\"id\"] + keys) | unique)" $file ' find-sh {} \;
