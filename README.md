# Wiki

[WORK IN PROGRESS roadmap #5 **Base collaborative de connaissances sur les notebooks**](https://gitlab.huma-num.fr/gt-notebook/roadmap/-/issues/5)
- Il s'agit de l'espace de production de notes semi-structurées,
- Le dépôt de [déploiement *NIX* des notes](https://gt-notebook.gitpages.huma-num.fr/nix/wiki-pandoc-nix/#home) est accessible [ici](https://gitlab.huma-num.fr/gt-notebook/nix).
