---
id: 20211102205928
title: "Stencila"
index:
type: permanente
doi:
url: https://stenci.la/
---
### Description

- **Mise à jour:** 02/11/2021 
- **Licence :** Stencila une solution Open Source, il propose un hébergement avec différents prix.
- **Fondations :** Stencila est cofondé par la fondation Sloan, Elife, CS&S, Coko, DatProject
- **Langage:** R et Python
- **Dépot FOSS:** https://github.com/stencila
- **Social:** https://twitter.com/stencila

- **Publication**

Il ont un partenariat important[^1] avec la revue ELife qui utilise leur solution pour ce qu'il apelle ERA (Executable Research Article). C'est une des brique de leur workflow de publication. Le journal GigaByte utilise également la solution ERA.

Stencila utilise une extension de Schema.org[^2], avec des bindings Python, R, Typescript

[Libero](https://libero.pub/products/libero-editor/) est cité comme editeur "user friendly" envisagé, celui-ci sera déployé avec un plugin Stencila (Encoda) pour une tranformation et une édition du document au format JATS XML. [^6]

- **Reproductibilité**

Des dépots (sources) peuvent être liés à un projet. Ces sources sont copiés et versionnés avec le projet Stencila pour des questions de reproductibilité. Il est possible de les mettre à jour avec le dépôt originel (procédure de Pull) manuellement.

Il est possible d'indiquer un Container à utiliser pour l'execution d'un projet.

Ils développent des outils afin d'améliorer encore la reproductibilité [^6] :  un créateur d'image Docker[^3], un package manager basé sur Nix[^4].  

La compatibilité de Stencila (complète ? partielle ? ) avec les capacités initiales de RMarkdown et Jupyter en natif n'est pas documenté. La lecture de la Roadmap 2021 [^6] est conseillé pour en savoir plus sur la vision des développeurs.

- **Données**

xxx work in progress xxx


### Interface/Desktop

L'usage premier est le web mais il est possible d'importer et de convertir sans perte des notebooks RMarkdown ou Jupyter fait en local.

Il existe un client Desktop en version alpha [^5]

### Usages

Il existe un [stencila-hub](https://hub.stenci.la/projects/) sur lequel des notebooks RMarkdown et Jupyter peuvent être uploadé. 
A partir de ce Hub, les utilisateurs peuvent visualiser les fichiers, les sources, ainsi que dupliquer (Fork) et lancer les papiers (Run).

L'interface permet le partage aussi entre collaborateurs, avec des dépôt privés et publics. Les collaborateurs peuvent ajouter des sources, des données, etc.

Une fois les sources définies et versionnés sur le serveur, que le  fichier principal est défini (Main), un snapshot doit être créé pour que l'article executable soit publié.

L'éditeur en ligne sur le web est assez basique …

L'aide est en cours de construction, pour avoir un aperçu il faut regarder les documents de références, par exemple pour [RMarkdown]( https://hub.stenci.la/stencila/reference-r-markdown/files/)

### Notes liées  

### Tags 
#opensource #initiatives

[^1]: https://elifesciences.org/labs/dc5acbde/welcome-to-a-new-era-of-reproducible-publishing
[^2]: https://help.stenci.la/references/schema 
[^3]: https://github.com/stencila/dockta
[^4]: https://github.com/stencila/nixta
[^5]: https://github.com/stencila/stencila/tree/master/desktop#%EF%B8%8F-stencila-desktop 
[^6]: https://elifesciences.org/labs/a04d2b80/announcing-the-next-phase-of-executable-research-articles

fin.

